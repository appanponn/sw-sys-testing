# sw-sys-testing

Repo to maintain notes of native mobile app, web app software testing and also system testing (incl. the hardware) in embedded scenarios.

## Systems & Software Applications

One can think of the following different sets of applications:

### Mobile App UI Test Automation

Some resources on how to get started on this:
1. [Appium tutorial for testing mobile apps](https://www.softwaretestinghelp.com/appium-tutorial-for-beginners/)
1. Need to be able to automate appinstalled from App Store
    * [Can I install app store apps on AppLive? (BrowserStack)](https://www.browserstack.com/question/726) 
1. [Mobile testing articles (guru99)](https://www.guru99.com/mobile-testing.html)
1. [TestComplete from Smartbear](https://smartbear.com/product/testcomplete/mobile-testing) - *Need to try the trial*.
1. [UI Testing - Android Developer](https://developer.android.com/training/testing/ui-testing)
1. [11 Best Automation Tools for Testing Android Applications](https://www.softwaretestinghelp.com/5-best-automation-tools-for-testing-android-applications/#3_TestComplete), SoftwareTestingHelp

### Web App UI Test Automation

TODO: Collect resources on tools for the following two levels of testing:

1. Testing the web UI in isolation by mocking the back-end invocations:
   This has to be done for the differnt popular libraries one can use for front-end, whcih are:
    * Angular - which are the latest versions?
    * React - which are the versions to support?
    * VueJS - what versions to support?
1. Testing the full end-to-end web app, including the back-end

### Desktop App Testing

Included in this category are the following specific set of apps:
1. Atom editor based cross-platform desktop apps
1. Desktop-native apps (TODO: find out specific ones & the platform API they use)
1. Extensions for VSCode and other developer tools

### Embedded Systems App Testing

This includes both hardware & app software that runs on those embedded hardware.

## Initial List of Tasks

* [ ] - Setup up laptop & mobile for trying out the instruction in ref. 1.1 above
* [ ] - Find out tools & tutorials for web app testing (already know a few things in this category)
* [ ] - Find out tools & tutorials for desktop-native app testing.
* [ ] - Unit & integration testing for embedded systems apps which can be distrubuted. 
